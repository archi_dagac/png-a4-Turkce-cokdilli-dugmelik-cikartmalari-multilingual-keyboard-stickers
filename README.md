## PNG-A4 Çok Dilli Düğmelik için - Türkçe-F - Köktürkçe - Rusça - Arapça - Soluk Türkçe-Q - Taslak ve Düğmelik Çıkartmaları

<br>
<br>
**Fonts:** 
<br><br>
"Candara" for Türkçe-F, Q, symbols, marks and Русский.
<br><br>
**Candara** 
<br>
_Category:_ Sans-serif<br>
_Designer(s):_ Gary Munch<br>
_Foundry:_ Munchfonts, Microsoft<br>
_License:_ Proprietary<br>
<br><br>
"Noto Naskh Arabic UI" for Arabic.
<br><br>
**Noto Naskh Arabic UI** 
<br>
_Classification:_ Sans-serif (humanist); serif (transitional); non-Latin<br>
_Commissioned by:_ Google<br>
_Date created:_ 2012–2014<br>
_Date released:_ 2013<br>
_License:_ SIL Open Font License<br>
_Website:_ www.google.com/get/noto/ <br>
<br>
<br>
"Tuğrul Çavdar 2019 Unicode yüzde15ince" for Köktürkçe.<br><br>
**Tuğrul Çavdar 2019 Unicode yüzde15ince** 
<br>
_Website:_ tamga.org<br>
_Date released:_ 2019<br>